package com.Softy.Services.GoodHub;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.MApps.Launcher.MiniUI.Mini;
import com.MApps.Launcher.MiniUI.R;
import com.MApps.Launcher.MiniUI.Sprite;

/**
 * Created by Admin on 4/30/2017.
 */

public class GoodHub extends Activity{
    private static String userText;
    private static String passText;
    private static boolean shouldOpen  = false;
    @Override
    public void onCreate(Bundle b)
    {
        super.onCreate(b);
        if(shouldOpen == true)
        {
            preLogin();
        }
    }

    public void preLogin()
    {
        getAppContent(R.layout.main_goodhub_page);
    }

    protected void getAppContent(int content)
    {
        setContentView(content);

        final AutoCompleteTextView user = getEditor(R.id.acct_user);
        final AutoCompleteTextView pass = getEditor(R.id.acct_pass);

        Button sign_up = getButton(R.id.sign_up);
        Button sign_in = getButton(R.id.sign_in);

        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog dialog = new ConfirmDialog(GoodHub.this);
                if(user.getText().equals("") && pass.getText().equals(""))
                {
                    Snackbar.make(user, "Both user and password can't be empty!", Snackbar.LENGTH_SHORT).show();
                }else if(user.getText().equals("") && !(pass.getText().equals("")))
                {
                    Snackbar.make(user, "User can't be empty!", Snackbar.LENGTH_SHORT).show();
                }else if(!(user.getText().equals("")) && pass.getText().equals(""))
                {
                    Snackbar.make(user, "Pass can't be empty!", Snackbar.LENGTH_SHORT).show();
                }else{
                    dialog.setUser(user.getText().toString());
                    dialog.setPass(pass.getText().toString());
                    dialog.show();
                    userText = user.getText().toString();
                    passText = pass.getText().toString();
                }
            }
        });

        sign_in.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConfirmDialog dialog = new ConfirmDialog(GoodHub.this);
                if (user.getText().equals("") && pass.getText().equals("")) {
                    Snackbar.make(user, "Both user and password can't be empty!", Snackbar.LENGTH_SHORT).show();
                } else if (user.getText().equals("") && !(pass.getText().equals(""))) {
                    Snackbar.make(user, "User can't be empty!", Snackbar.LENGTH_SHORT).show();
                } else if (!(user.getText().equals("")) && pass.getText().equals("")) {
                    Snackbar.make(user, "Pass can't be empty!", Snackbar.LENGTH_SHORT).show();
                } else {
                    LoginTask loginTask = new LoginTask(getApplicationContext(), user.getText().toString(), pass.getText().toString());
                    loginTask.execute();
                    userText = user.getText().toString();
                    passText = pass.getText().toString();
                    //for this app only
                    getPackageManager().setComponentEnabledSetting(new ComponentName(GoodHub.this, Mini.class), PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                    getPackageManager().setComponentEnabledSetting(new ComponentName(GoodHub.this, Sprite.class), PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
                    Intent main = new Intent(Intent.ACTION_MAIN);
                    main.addCategory(Intent.CATEGORY_HOME);
                    startActivity(main);
                }
            }
        });
    }


    protected void afterLogin()
    {
        Log.i("LoginDone", "User has logged in");
    }

    private Button getButton(int contentId)
    {
        return (Button) findViewById(contentId);
    }

    private AutoCompleteTextView getEditor(int contentId)
    {
        return (AutoCompleteTextView) findViewById(contentId);
}

    public static String getUser()
    {
        return userText;
    }

    public static String getPass()
    {
        return passText;
    }

    public static void launchWhenOpen(boolean shouldOpen) {
        GoodHub.shouldOpen = shouldOpen;
    }
}
