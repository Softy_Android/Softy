package com.Softy.Services.GoodHub;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.Build;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Admin on 4/30/2017.
 */

public class RegisterTask extends AsyncTask<Void, Void, String> {
    private static Context mContext;
    private static String email;
    private static String user;
    private static String pass;
    private static String verify;

    public RegisterTask(Context mContext, String email, String user, String pass, String verify)
    {
        RegisterTask.mContext = mContext;
        RegisterTask.email = email;
        RegisterTask.user = user;
        RegisterTask.pass = pass;
        RegisterTask.verify = verify;
    }
    @Override
    protected String doInBackground(Void... params) {
        String result = "";
        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost("https://goodhub.000webhostapp.com/register.php");
            List<NameValuePair> list = new ArrayList<>(4);
            list.add(new BasicNameValuePair("user", user));
            list.add(new BasicNameValuePair("uemail", email));
            list.add(new BasicNameValuePair("pass", pass));
            list.add(new BasicNameValuePair("pass_verify", verify));
            post.setEntity(new UrlEncodedFormEntity(list));

            //execute
            HttpResponse response = client.execute(post);
            HttpEntity entity = response.getEntity();
            InputStream is = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),5);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null)
            {
                sb.append(line);
            }
            is.close();
            result = sb.toString();
        }catch(ClientProtocolException cpe)
        {
        }catch(IOException ioe)
        {
        }
        return result;
    }

    @Override
    public void onPostExecute(String endResult)
    {
        try{
            File prop = new File("/sdcard/res.prop");
            if(!(new File("/sdcard/MiniUI").exists())){
                new File("/sdcard/MiniUI").mkdirs();
            }
            FileWriter fw = null;
            if(prop.exists()){
                fw = new FileWriter(prop, true);
            }else{
                fw = new FileWriter(prop);
            }

            fw.write(endResult);
            fw.flush();
            fw.close();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        Toast.makeText(mContext, endResult, Toast.LENGTH_LONG).show();
    }
}
