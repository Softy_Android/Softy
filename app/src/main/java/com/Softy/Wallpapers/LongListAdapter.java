package com.Softy.Wallpapers;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.MApps.Launcher.MiniUI.R;

/**
 * Created by softy on 6/11/17.
 */

public class LongListAdapter extends BaseAdapter
{
    private static Context mContext;
    private static Walls[] walls;
    public LongListAdapter(Context mContext, Walls walls[])
    {
        this.mContext = mContext;
        this.walls = walls;
    }
    @Override
    public int getCount() {
        return walls.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    static class ImageHolder
    {
        ImageView image;
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater li = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ImageHolder iHolder;
        if( convertView == null )
        {
            convertView = li.inflate(R.layout.wallpaper_preview, null, false);
            iHolder = new ImageHolder();
            iHolder.image = (ImageView) convertView.findViewById(R.id.wallpaper_preview);

            convertView.setTag(iHolder);
        }else
            iHolder = (ImageHolder) convertView.getTag();

        iHolder.image.setImageBitmap(walls[position].bitmap);
        return convertView;
    }
}