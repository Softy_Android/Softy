package com.Softy.Wallpapers;
import android.app.DownloadManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

//Import download manager

public class Wallpaper
{
    private Context mContext;
    public Wallpaper(Context mContext)
    {
        this.mContext = mContext;
    }
    private DownloadImage image;
    public void setDownloader(DownloadImage imageDownloader)
    {
        if ( imageDownloader == null )
        {
            try {
                throw new Exception("DownloadImage must not be null");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        image = imageDownloader;
    }
    public void start()
    {
        image.downloadImage(image.getImageURL(), image.getImagePath());
    }
    public static class DownloadImage
    {
        private Context mContext;
        public DownloadImage(Context myContext)
        {   mContext = myContext;   }
        private String imagePath;
        private String storagePath;
        private String imageURL;
        private String title;
        private static View view;

        public void setView(View mView)
        {
            view = mView;
        }
        public void setStoragePath(String mStoragePath)
        {
            this.storagePath = mStoragePath;
        }
        public void setImagePath(String mImagePath)
        {
            if( ! mImagePath.startsWith("/sdcard") || ! mImagePath.startsWith("/storage"))
            {
                Log.i("Root","Not downloading image to any other path");
                try {
                    throw new Exception("Not downloading image to other path");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            this.imagePath = mImagePath;
        }
        public String getImagePath()
        {
            //Get image path, typically some temporary directory
            return imagePath;
        }
        public void setImageTitle(String title) { this.title = title; }
        public void setImageURL(String url) { this.imageURL = url; }
        public String getImageURL() { return this.imageURL; }
        public String getImageTitle() { return title; }
        public void downloadImage(String url, String imagePath)
        {
            //Use DownloadManager to download image from url
            //Then set the image as background
            boolean isViewSet = true;
            if ( view == null )
            {
                isViewSet = false;
                Log.i("NoViewSet","Using Toast instead of Snackbar");
            }

            if(isViewSet)
                Snackbar.make(view, "Downloading image from server", Snackbar.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "Downloading image from server", Toast.LENGTH_SHORT).show();


            if(isViewSet)
                Snackbar.make(view, "Finished downloading wallpaper", Snackbar.LENGTH_SHORT).show();
            else
                Toast.makeText(mContext, "Finished downloading wallpaper", Toast.LENGTH_SHORT).show();

            Bitmap image = BitmapFactory.decodeFile(getImagePath());
            try {
                mContext.setWallpaper(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}