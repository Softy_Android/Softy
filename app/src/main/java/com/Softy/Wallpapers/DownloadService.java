package com.Softy.Wallpapers;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by softy on 6/11/17.
 */

public class DownloadService extends AsyncTask<String, Void, String>{
    public static String TITLE;
    public static String PATH;
    public static String mURL;
    public static String DOWNLOAD_URL="https://softy.000webhostapp.com/wallpapers/index.php";
    private InputStream is;
    private String result;
    private JSONArray ja;
    private List<String> qs;
    private List<String> as;
    private static Context mContext;
    private static ListView list;
    private Walls q[];
    public DownloadService setContext(Context context){
        mContext = context;
        return this;
    }
    public DownloadService setListView(ListView list){
        this.list = list;
        return this;
    }
    @Override
    protected String doInBackground(String... params) {
        try{
            HttpClient hc = new DefaultHttpClient();
            HttpPost hp = new HttpPost(DOWNLOAD_URL);
            HttpResponse hr = hc.execute(hp);
            HttpEntity he = hr.getEntity();
            is = he.getContent();
        }catch (Exception e){
            result = e.getMessage();
            e.printStackTrace();
        }
        try{
            BufferedReader br = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null){
                sb.append(line);
            }
            is.close();
            result = sb.toString();
        }catch (Exception e){
            result = e.getMessage();
            e.printStackTrace();
        }

        return result;
    }

    @Override
    public void onPostExecute(String s){
        Toast.makeText(mContext, s, Toast.LENGTH_SHORT).show();
        LongListAdapter lla = null;
        try{
            Toast.makeText(mContext, result, Toast.LENGTH_SHORT).show();
            ja = new JSONArray(s);
            String line;
            q = new Walls[ja.length()];
            for(int i = 0; i < ja.length(); i++){
                q[i] = new Walls();
                JSONObject jo = ja.getJSONObject(i);
                qs = new ArrayList<>();
                as = new ArrayList<>();
                TITLE = jo.getString("title");
                mURL = jo.getString("url");
                q[i].TITLE = TITLE;
                q[i].URL = mURL;
                URL url = new URL(mURL);
                q[i].bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream());
                lla = new LongListAdapter(mContext,q);

                list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Wallpaper.DownloadImage mImage = new Wallpaper.DownloadImage(mContext);
                        mImage.setImagePath("/sdcard/MINI-WALLS/");
                        mImage.setImageTitle(TITLE);
                        mImage.setImageURL(mURL);
                        mImage.setView(list);

                        Wallpaper mWallpaper = new Wallpaper(mContext);
                        mWallpaper.setDownloader(mImage);
                        mWallpaper.start();
                    }
                });
            }
        }catch (Exception e){
            Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
        list.setAdapter(lla);
    }


}