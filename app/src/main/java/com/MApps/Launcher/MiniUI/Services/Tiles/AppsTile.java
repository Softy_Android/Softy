package com.MApps.Launcher.MiniUI.Services.Tiles;

import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.service.quicksettings.TileService;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.HapticFeedbackConstants;
import android.view.LayoutInflater;
import android.view.WindowManager;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.Build;
import com.MApps.Launcher.MiniUI.Mini;
import com.MApps.Launcher.MiniUI.R;
import com.lb.launcher.AppsCustomizePagedView;
import com.lb.launcher.Launcher;
import com.lb.launcher.LauncherRootView;

/**
 * Created by Admin on 4/28/2017.
 */

public class AppsTile extends TileService{
    private WindowManager wm;
    @Override
    public void onTileAdded()
    {
        Log.i("TileAdded", "User added apps tile");
    }

    @Override
    public void onStartListening()
    {
        Log.i("Listening", "Listening for press events");
    }

    @Override
    public void onClick() {
        handleWindows();
    }



    private void handleWindows()
    {

        Intent i = new Intent(this, Mini.class);
        Mini.previewApps(true);
        startActivity(i);
    }

    @Override
    public void onTileRemoved()
    {
        Log.e("TileRemoved", "User removed apps tile");
    }
}
