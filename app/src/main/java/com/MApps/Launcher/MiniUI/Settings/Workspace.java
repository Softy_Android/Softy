package com.MApps.Launcher.MiniUI.Settings;

import android.content.Context;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.R;

/**
 * Created by Admin on 5/6/2017.
 */

public class Workspace extends PreferenceActivity implements Preference.OnPreferenceClickListener
{
    @Override
    public void onCreate(Bundle bundle)
    {
        String theme = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).getString(Data.TEMP_THEME, "");
        switch(theme)
        {
            case Data.LIGHT:
                setTheme(R.style.Light);
                break;

            case Data.DARK:
                setTheme(R.style.Dark);
                break;

            case "":
                setTheme(R.style.Dark);
                break;
        }
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.workspace);
    }

    public void setToolbar(@Nullable Toolbar toolbar)
    {
        get().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate acd;
    public AppCompatDelegate get()
    {
        if(acd == null)
        {
            acd = AppCompatDelegate.create(this, null);
        }
        return acd;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

}