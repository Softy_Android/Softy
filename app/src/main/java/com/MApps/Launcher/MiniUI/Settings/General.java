package com.MApps.Launcher.MiniUI.Settings;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.R;
import com.lb.launcher.IconCache;
import com.lb.launcher.LauncherBackupHelper;
import com.lb.launcher.ShortcutInfo;
import com.lb.launcher.themes.IconPackManager;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by Admin on 5/6/2017.
 */

public class General extends PreferenceActivity implements Preference.OnPreferenceClickListener
{
    private static int REQUEST_PACKS = 65535;
    @Override
    public void onCreate(Bundle bundle)
    {
        String theme = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).getString(Data.TEMP_THEME, "");
        switch(theme)
        {
            case Data.LIGHT:
                setTheme(R.style.Light);
                break;

            case Data.DARK:
                setTheme(R.style.Dark);
                break;

            case "":
                setTheme(R.style.Dark);
                break;
        }
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.general);
        final CheckBoxPreference dark = (CheckBoxPreference) findPreference("dark_theme");
        final CheckBoxPreference light = (CheckBoxPreference) findPreference("light_theme");

        dark.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {

                getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putString(Data.TEMP_THEME, Data.DARK).commit();
                light.setChecked(false);
                Toast.makeText(getApplicationContext(), "Applied dark theme", Toast.LENGTH_SHORT);
                return false;
            }
        });
        light.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putString(Data.TEMP_THEME, Data.LIGHT).commit();
                dark.setChecked(false);
                Toast.makeText(getApplicationContext(), "Applied light theme", Toast.LENGTH_SHORT);
                return false;
            }
        });

        ((SwitchPreference) findPreference("qsb")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(preference instanceof  SwitchPreference)
                {
                    SwitchPreference mSwitch = (SwitchPreference) preference;
                    if((mSwitch.isChecked()))
                    {
                        getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putBoolean(Data.Qsb.QSB_ON, true).commit();
                        Toast.makeText(getApplicationContext(), "Showing Google Search Bar", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putBoolean(Data.Qsb.QSB_ON, false).commit();
                        Toast.makeText(getApplicationContext(), "Hiding Google Search Bar", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        ((Preference) findPreference("icon_pack")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Intent intent = new Intent(Intent.ACTION_PICK_ACTIVITY);



                Intent filter = new Intent(Intent.ACTION_MAIN);

                filter.addCategory("com.anddoes.launcher.THEME");



                intent.putExtra(Intent.EXTRA_INTENT, filter);


                startActivityForResult(intent, REQUEST_PACKS);
                return false;
            }
        });
    }

    public void setToolbar(@Nullable Toolbar toolbar)
    {
        get().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate acd;
    public AppCompatDelegate get()
    {
        if(acd == null)
        {
            acd = AppCompatDelegate.create(this, null);
        }
        return acd;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {

        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent main)
    {
        if(requestCode == REQUEST_PACKS && resultCode == RESULT_OK)
        {

        }
        super.onActivityResult(requestCode,resultCode,main);
    }


}