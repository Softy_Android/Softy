package com.MApps.Launcher.MiniUI;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Icon;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.service.quicksettings.TileService;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.Build;
import com.MApps.Launcher.MiniUI.Receivers.a;
import com.MApps.Launcher.MiniUI.Services.as;
import com.Softy.Services.GoodHub.LoginTask;
import com.lb.launcher.AppsCustomizePagedView;
import com.lb.launcher.Folder;
import com.lb.launcher.FolderIcon;
import com.lb.launcher.Launcher;

public class Mini extends Launcher {
    private a a;
    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);

        try {

            doLaunch(b);

            Build.Hardware.writeHardware(this);
            //addAnim();
            Toast.makeText(this,LoginTask.getCookieValue("user", "https://goodhub.000webhostapp.com/home.php"),Toast.LENGTH_SHORT);
            if(shouldPreviewApps == true)
            {
                showAllApps(true, AppsCustomizePagedView.ContentType.Applications, true);
            }

            Intent i = new Intent("softy.intent.action.APPLY_LAUNCHER_SETTINGS");
            i.setPackage(this.getPackageName());
            startService(i);

            Intent is = new Intent(this, as.class);
            startService(is);

            IntentFilter filter = new IntentFilter();
            filter.addAction("softy.intent.action.START_APPLY_LAUNCHER_SETTINGS");

            registerReceiver(new a(), filter);
        }catch(Exception e)
        {
            Log.e("UnknownError", e.getMessage());
            Toast.makeText(this, "An error occurred. ", Toast.LENGTH_SHORT).show();
            onCreate(b);
        }
    }

    public void addAnim()
    {
        CustomContentCallbacks ccc = new CustomContentCallbacks() {
            @Override
            public void onShow(boolean fromResume) {
                Toast.makeText(Mini.this, "Showing animation", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onHide() {

            }

            @Override
            public void onScrollProgressChanged(float progress) {

            }

            @Override
            public boolean isScrollingAllowed() {
                return false;
            }
        };
        addToCustomContentPage((RelativeLayout) getLayoutInflater().inflate(R.layout.sprite_animation, null), ccc, "Sprite");
    }

    private static boolean shouldPreviewApps = false;
    public static boolean previewApps(boolean shouldPreview)
    {
        Mini.shouldPreviewApps = shouldPreview;
        return shouldPreview;
    }
    @Override
    public void onResume()
    {
        super.onResume();

        //write hardware information for debugging purposes
        Build.Hardware.writeHardware(this);

        Intent i = new Intent("softy.intent.action.APPLY_LAUNCHER_SETTINGS");
        i.setPackage(this.getPackageName());
        startService(i);

        IntentFilter filter = new IntentFilter();
        filter.addAction("softy.intent.action.START_APPLY_LAUNCHER_SETTINGS");

        registerReceiver(new a(), filter);
    }
}
