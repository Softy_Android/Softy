package com.MApps.Launcher.MiniUI.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.Templates;
import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.Mini;
import com.MApps.Launcher.MiniUI.R;
import com.MApps.Launcher.MiniUI.Template;
import com.lb.launcher.AppsCustomizePagedView;
import com.lb.launcher.AppsCustomizeTabHost;
import com.lb.launcher.Launcher;
import com.lb.launcher.LauncherRootView;

import java.io.File;
import java.io.IOException;

/**
 * Created by Admin on 4/19/2017.
 */

public class as extends Service{
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent i, int f, int s)
    {
        try
        {

            int back = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).getInt(Data.Drawer.DRAWER_BACKGROUND, 0);
            int fore = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).getInt(Data.Drawer.DRAWER_FOREGROUND, 0);

            if(back == 0)
            {
                Launcher.mAppsCustomizeTabHost.setBackgroundColor(back);
            }else if(fore == 0)
            {
                Launcher.mAppsCustomizeContent.setBackgroundColor(fore);
            }else{
                Launcher.mAppsCustomizeTabHost.setBackgroundColor(back);
                Launcher.mAppsCustomizeContent.setBackgroundColor(fore);
            }

            fuckingNotify();
        }catch(Exception ioe)
        {
            ioe.printStackTrace();
        }
        return super.onStartCommand(i, s, f);
    }

    public Context getThis()
    {
        return getApplicationContext();
    }

    public SharedPreferences getShared()
    {
        return getThis().getSharedPreferences(Data.NAME, Context.MODE_PRIVATE);
    }

    public String getString(String key)
    {
        return getShared().getString(key, "");
    }

    public int getInt(String key)
    {
        return getShared().getInt(key, 0);
    }

    public boolean getBoolean(String key)
    {
        return getShared().getBoolean(key, true);
    }

    public void isColorDark(int color, AppsCustomizePagedView mAppsCustomizePaged){
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        if(darkness<0.5){
            ((AppsCustomizePagedView)mAppsCustomizePaged.findViewById(R.id.apps_customize_pane)).getIcon().setTextColor(Color.BLACK);
        }else{
            ((AppsCustomizePagedView)mAppsCustomizePaged.findViewById(R.id.apps_customize_pane)).getIcon().setTextColor(Color.WHITE);
        }
    }
    public void fuckingNotify()
    {
        listenForFiles("/sdcard/MiniUI/Templates");
    }

    public void listenForFiles(String path) {
        File main = new File(path);
        if (main.exists()) {
            File[] list = main.listFiles();
            for (File f : list) {
                if (f.isFile()) {
                    if (f.getName().endsWith(".inf")) {
                        Templates template = new Templates(getThis());
                        String title = template.getTemplateName(f.getPath());
                        Toast.makeText(getThis(), "New template available!", Toast.LENGTH_SHORT).show();
                        notify("Launcher template " + title, "Tap to open installer for " + title);
                    }
                } else {
                    listenForFiles(f.getPath());
                }
            }
        }
    }

    public Notification notify(String title, String text) {
        PendingIntent pi = PendingIntent.getActivity(getThis(), 0, new Intent(getThis(), Template.class), PendingIntent.FLAG_UPDATE_CURRENT);
        Notification n = new Notification.Builder(getThis())
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(android.R.drawable.ic_menu_add)
                .setContentIntent(pi)
                .build();

        NotificationManager nm = (NotificationManager) getThis().getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(10, n);
        return n;
    }
}
