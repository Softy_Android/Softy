package com.MApps.Launcher.MiniUI.Receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.BroadcastReceiver;
import com.MApps.Launcher.MiniUI.Classes.Templates;
import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.R;
import com.MApps.Launcher.MiniUI.Template;

import java.io.File;

/**
 * Created by Admin on 4/19/2017.
 */

public class b extends BroadcastReceiver {

    @Override
    public void onReceive(Context c, Intent i)
    {
        listenForFiles("/sdcard/MiniUI/Templates");
    }

    public void listenForFiles(String path) {
        File main = new File(path);
        if (main.exists()) {
            File[] list = main.listFiles();
            for (File f : list) {
                if (f.isFile()) {
                    if (f.getName().endsWith(".inf")) {
                        Templates template = new Templates(getThis());
                        String title = template.getTemplateName(f.getPath());
                        Toast.makeText(getThis(), "New template available!", Toast.LENGTH_SHORT).show();
                        notify("Launcher template " + title, "Tap to open installer for " + title);
                    }
                } else {
                    listenForFiles(f.getPath());
                }
            }
        }
    }

    public Notification notify(String title, String text) {
        PendingIntent pi = PendingIntent.getActivity(getThis(), 0, new Intent(getThis(), Template.class), PendingIntent.FLAG_UPDATE_CURRENT);
        Notification n = new Notification.Builder(getThis())
                .setContentTitle(title)
                .setContentText(text)
                .setSmallIcon(android.R.drawable.ic_menu_add)
                .setContentIntent(pi)
                .build();

        NotificationManager nm = (NotificationManager) getThis().getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(10, n);
        return n;
    }

}