package com.MApps.Launcher.MiniUI;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Environment;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.Templates;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mcom on 2/21/17.
 */

public class Template extends PreferenceActivity{

    @Override
    public void onCreate(Bundle b){
        super.onCreate(b);
        addPreferencesFromResource(R.xml.templates);
        final PreferenceCategory mList = (PreferenceCategory) findPreference(getResources().getString(R.string.templates_category));
        listTemplatesAt(mList, Environment.getExternalStorageDirectory().getPath()+"/MiniUI/Templates/");
        final Preference down = new Preference(this);
        down.setTitle("Download sample template");
        down.setSummary("Download sample template from app");
        down.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                copyAsset("INF/Default.inf", "/sdcard/MiniUI/Templates/Template/INF/");
                if(new File("/sdcard/MiniUI/Templates/Template/INF/Default.inf").exists())
                {
                    Toast.makeText(Template.this, "Downloaded template", Toast.LENGTH_SHORT).show();
                }
                mList.removePreference(down);
                listTemplatesAt(mList, Environment.getExternalStorageDirectory().getPath()+"/MiniUI/Templates/");
                mList.addPreference(down);
                return false;
            }
        });
        //mList.addPreference(down);
    }
    public void listTemplatesAt(final PreferenceCategory mList, String mPath){
        File f = new File(mPath);
        if(f.exists() && f.isDirectory()){
            final File[] path = f.listFiles();
            for(final File file : path){
                if(file.isFile()){
                    if(file.getName().endsWith(".inf")){
                        final Templates mTemp = new Templates(getApplicationContext());
                        final List<String> name = new ArrayList<>();
                        name.add(mTemp.getTemplateName(file.getPath()));
                        Preference mPreference = new Preference(getApplicationContext());
                        mPreference.setTitle(mTemp.getTemplateName(file.getPath()));
                        mPreference.setKey(mTemp.getTemplateName(file.getPath()).toLowerCase());
                        mPreference.setSummary(mTemp.getSummary(file.getPath()));
                        mPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                            @Override
                            public boolean onPreferenceClick(Preference preference) {
                                SharedPreferences sharedPrefs = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE);
                                SharedPreferences.Editor e = sharedPrefs.edit();
                                mTemp.showMessage("Setting "+name.get(0)+ "'s data as default.");
                                if(mTemp.getDrawerBackground(file.getPath()).equals("")){
                                    int color = Color.WHITE;
                                    e.putInt(Data.Drawer.DRAWER_BACKGROUND, color);
                                    /**Launcher.mAppsCustomizeTabHost.setBackgroundColor(color);**/
                                    isColorDark(color, Data.Drawer.DRAWER_BACKGROUND);
                                }else{
                                    //e.putInt(Data.Drawer.DRAWER_BACKGROUND, mTemp.getDrawerBackground(file.getPath()));
                                    String mColorText = mTemp.getDrawerBackground(file.getPath());
                                    if(mColorText.contains(",")) mTemp.showMessage("I ony support hex. :/");
                                    int color = Color.parseColor(mColorText);
                                    int r = Color.red(color);
                                    int g = Color.green(color);
                                    int b = Color.blue(color);
                                    e.putInt(Data.Drawer.DRAWER_BACKGROUND, Color.rgb(r, g, b));
                                    /**Launcher.mAppsCustomizeTabHost.setBackgroundColor(Color.rgb(r, g, b));**/
                                    isColorDark(Color.rgb(r, g, b), Data.Drawer.DRAWER_BACKGROUND);
                                }
                                if(mTemp.getDrawerForeground(file.getPath()).equals("")){
                                    int color = Color.WHITE;
                                    e.putInt(Data.Drawer.DRAWER_BACKGROUND, color);
                                    /**Launcher.mAppsCustomizeContent.mLayout.setBackgroundColor(color);
                                    Launcher.mAppsCustomizeContent.setBackgroundColor(color);
                                    Launcher.fake_page_container.setBackgroundColor(color);
                                    Launcher.fake_page.setBackgroundColor(color);**/
                                    isColorDark(color, Data.Drawer.DRAWER_FOREGROUND);
                                }else{
                                    //e.putInt(Data.Drawer.DRAWER_BACKGROUND, mTemp.getDrawerBackground(file.getPath()));

                                    String mColorText = mTemp.getDrawerForeground(file.getPath());
                                    if(mColorText.contains(",")) mTemp.showMessage("I ony support hex. :/");
                                    int color = Color.parseColor(mColorText);
                                    int r = Color.red(color);
                                    int g = Color.green(color);
                                    int b = Color.blue(color);
                                    e.putInt(Data.Drawer.DRAWER_FOREGROUND, Color.rgb(r, g, b));

                                    isColorDark(Color.rgb(r, g, b), Data.Drawer.DRAWER_FOREGROUND);

                                }


                                e.commit();
                                return false;
                            }
                        });
                        mList.addPreference(mPreference);
                    }else if(file.getName().endsWith(".png")){

                    }else{
                        Preference mPreference = new Preference(getApplicationContext());
                        mPreference.setTitle("No template found. Reload?");
                        mPreference.setKey("no-template");
                        mPreference.setSummary("I could not find any templates.");
                        mList.addPreference(mPreference);
                    }
                }else{
                    listTemplatesAt(mList,file.getPath());
                }
            }
        }
    }
    public void isColorDark(int color, String preference){
        getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putInt(preference, color).commit();
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        if(darkness<0.5){
            getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putInt(Data.DRAWER_TEXT_COLOR, Color.BLACK).commit();
        }else{
            getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putInt(Data.DRAWER_TEXT_COLOR, Color.WHITE).commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuItem down = menu.add("Download template");
        down.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                copyAsset("INF/Default.inf", "/sdcard/MiniUI/Templates/Template/INF");
                if(new File("/sdcard/MiniUI/Templates/Template/INF/Default.inf").exists())
                {
                    Toast.makeText(Template.this, "Downloaded template", Toast.LENGTH_SHORT).show();
                }
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }

    private void copyAsset(String name, String path) {

        File folder = new File(path);
        if(!folder.exists())
            folder.mkdirs();

        AssetManager assetManager = getAssets();
        String[] files = null;
        InputStream in = null;
        OutputStream out = null;
        try
        {
            in = assetManager.open(name);
            out = new FileOutputStream(path);
            copy(in, out);
            in.close();
            in = null;
            out.flush();
            out.close();
            out = null;
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
    }

    private void copy(InputStream in, OutputStream out) throws IOException
    {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1)
        {
            out.write(buffer, 0 ,read);
        }
    }


}

