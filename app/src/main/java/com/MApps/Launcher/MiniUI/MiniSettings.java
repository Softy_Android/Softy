package com.MApps.Launcher.MiniUI;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceScreen;
import android.preference.SwitchPreference;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Window;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Settings.Drawer;
import com.MApps.Launcher.MiniUI.Settings.General;
import com.MApps.Launcher.MiniUI.Settings.Workspace;

/**
 * Created by mcom on 1/22/17.
 */

public class MiniSettings extends PreferenceActivity implements Preference.OnPreferenceClickListener {
    private static AppCompatDelegate acd;
    @Override
    public void onCreate(Bundle bundle)
    {
        final SharedPreferences sharedPrefs = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE);
        String theme = sharedPrefs.getString(Data.TEMP_THEME, "");
        switch(theme)
        {
            case Data.LIGHT:
                setTheme(R.style.Light);
                break;

            case Data.DARK:
                setTheme(R.style.Dark);
                break;

            case "":
                setTheme(R.style.Dark);
                break;
        }
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.settings);
        ((Preference) findPreference("workspace")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                launch(Workspace.class);
                return false;
            }
        });

        ((Preference) findPreference("general")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                launch(General.class);
                return false;
            }
        });

        ((Preference) findPreference("drawer")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                launch(Drawer.class);
                return false;
            }
        });

        ((SwitchPreference) findPreference("dock")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(preference instanceof SwitchPreference)
                {
                    SwitchPreference mSwitch = (SwitchPreference) preference;
                    if(!(mSwitch.isChecked()))
                    {
                        sharedPrefs.edit().putBoolean(Data.Dock.DOCK_ON, false).commit();
                        Toast.makeText(getApplicationContext(), "Hiding dock", Toast.LENGTH_SHORT).show();
                    }else
                    {
                        sharedPrefs.edit().putBoolean(Data.Dock.DOCK_ON, true).commit();
                        Toast.makeText(getApplicationContext(), "Showing dock", Toast.LENGTH_SHORT).show();
                    }
                }
                return false;
            }
        });

        ((Preference) findPreference("version")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                try {
                    PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), 0);
                    int version = info.versionCode;
                    String name = info.versionName;
                    Toast.makeText(MiniSettings.this, "Version: "+version+"\n Code Name: "+name, Toast.LENGTH_SHORT).show();
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                return false;
            }
        });

        ((CheckBoxPreference) findPreference("developer_options")).setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                if(preference instanceof CheckBoxPreference)
                {
                    CheckBoxPreference check = (CheckBoxPreference) preference;
                    if(!(check.isChecked()))
                    {
                        sharedPrefs.edit().putBoolean("dev_ops_enabled", false).commit();
                        showMessage("Disabled developer options");
                    }else
                    {
                        sharedPrefs.edit().putBoolean("dev_ops_enabled", true).commit();
                        showMessage("Enabled developer options");
                        PreferenceScreen mainScreen = (PreferenceScreen) findPreference("screen_base");
                        PreferenceCategory mDevOps = getDeveloperOptions();
                        mainScreen.addPreference(mDevOps);
                    }
                }
                return false;
            }
        });

        if(sharedPrefs.getBoolean("dev_ops_enabled", true))
        {
            PreferenceScreen mainScreen = (PreferenceScreen) findPreference("screen_base");
            PreferenceCategory mDevOps = getDeveloperOptions();
            mainScreen.addPreference(mDevOps);
        }
    }

    public PreferenceCategory getDeveloperOptions()
    {
        PreferenceCategory mDevOps = new PreferenceCategory(this);
        mDevOps.setTitle("Developer options");
        mDevOps.setKey("dev_ops");

        SwitchPreference mLog = new SwitchPreference(this);
        mLog.setTitle("Refresh application");
        mLog.setKey("refresh");

        final SharedPreferences sharedPrefs = getSharedPreferences(Data.NAME, MODE_PRIVATE);

        mDevOps.addPreference(mLog);
        return mDevOps;
    }

    private void showMessage(String s) {
        Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
    }

    private void launch(Class myActivity) {
        Intent i = new Intent(this, myActivity);
        startActivity(i);
    }

    public void setToolbar(@Nullable Toolbar toolbar)
    {
        get().setSupportActionBar(toolbar);
    }

    public AppCompatDelegate get()
    {
        if(acd == null)
        {
            acd = AppCompatDelegate.create(this, null);
        }
        return acd;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

}
