package com.MApps.Launcher.MiniUI.Classes;

import android.graphics.drawable.Drawable;

/**
 * Created by mcom on 3/29/17.
 */

public class Apps {
    public String label;
    public String name;
    public String packageName;
    public Drawable mIcon;
}
