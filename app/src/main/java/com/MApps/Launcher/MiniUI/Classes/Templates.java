package com.MApps.Launcher.MiniUI.Classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Data;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by mcom on 2/21/17.
 */

public class Templates {
    private Context mContext;
    public Templates(Context mContext){
        this.mContext = mContext;
    }
    public void unzipTemplates(String dir, String to, boolean isList){
        boolean areTemplatesUnzipped = unzip(dir, to,isList);
    }
    private boolean unzip(String dir, String to, boolean isList){
        if(isList){
            return unzipList(dir);
        }else{
            return extractIndiv(dir, to);
        }
    }

    private boolean unzipList(String dir){
        File main = new File(dir);
        if(main.exists())
        {
            File[] list = main.listFiles();
            for(int i = 0; i < list.length; i++)
            {
                File f = list[i];
                String name = f.getName();
                String path = f.getPath();
                String parentPath = f.getParentFile().getPath();
                if(f.isFile()){
                    if(name.endsWith(".mini")){
                        File newDir = new File(path.replace(name, "") + "/" + name.replace(".mini", ""));
                        if(!newDir.exists())
                            newDir.mkdir();

                        return extractIndiv(path, newDir.getPath());
                    }
                }
            }
        }
        return false;
    }

    private boolean extractIndiv(String from, String to){
        boolean isUnzipped = false;
        InputStream is;
        ZipInputStream zis;
        try
        {
            String name;
            is = new FileInputStream(from);
            zis = new ZipInputStream(new BufferedInputStream(is));
            ZipEntry ze = null;
            byte[] buffer = new byte[1024];
            int count;

            while( (ze = zis.getNextEntry()) != null){
                //set file name
                name = ze.getName();
                if(ze.isDirectory()){
                    File fmd = new File(to);
                    fmd.mkdir();
                }

                FileOutputStream fout = new FileOutputStream(to);

                while((count =zis.read(buffer)) != -1){
                    fout.write(buffer,0, count);
                    isUnzipped = true;
                }
                fout.close();
                zis.closeEntry();
            }
            zis.close();
        }catch(IOException ioe)
        {
            ioe.printStackTrace();
            isUnzipped = false;
        }
        return isUnzipped;
    }


    public String getTemplateName(String path){
        String name = "Zara";
        File info = new File(path);
        try
        {
            BufferedReader br = new BufferedReader(new FileReader(info));
            String line;
            while((line = br.readLine()) != null)
            {
                 if(line.contains("template.name=")){
                     name = line.replace("template.name=", "");
                     return line.replace("template.name=", "");
                 }
            }
        }
        catch(IOException ioe)
        {
            ioe.printStackTrace();
        }
        return name;
    }

    public Bitmap getWallpaper(String path){
        Bitmap wall = null;
        File main = new File(path);
        String paper = null;
        try{
            BufferedReader br = new BufferedReader(new FileReader(main));
            String line;
            while((line = br.readLine()) != null){
                if(line.contains("template.wallpaper=")){
                    paper = line.replace("template.wallpaper=","");
                    String newLine = "";
                    if(!(paper.startsWith("/"))) {
                        newLine = Environment.getExternalStorageDirectory().getPath() + "/" + paper;
                        showMessage("."+newLine);
                    }
                    wall = BitmapFactory.decodeFile(newLine);
                }else{
                    wall = ((BitmapDrawable) mContext.getWallpaper()).getBitmap();
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        return wall;
    }

    public String getWallpaperLocation(String path){
        File main = new File(path);
        String paper = null;
        try{
            BufferedReader br = new BufferedReader(new FileReader(main));
            String line;
            while((line = br.readLine()) != null){
                if(line.contains("template.wallpaper=")){
                    paper = line.replace("template.wallpaper=","");
                    //Toast.makeText(mContext, paper, Toast.LENGTH_SHORT).show();
                }else{
                    paper = "Default";
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
        return paper;
    }


    public void showMessage(String message){
        Toast.makeText(mContext, message, Toast.LENGTH_SHORT).show();
    }

    public String getSummary(String path) {
        String result = "";
        File main = new File(path);
        try{
            BufferedReader br = new BufferedReader(new FileReader(main));
            String line;
            while((line = br.readLine()) != null){
                if(line.contains("template.summary=")){
                    result = line.replace("template.summary=","");
                    return line.replace("template.summary=", "");
                }
            }
        }catch(IOException ere){
            ere.printStackTrace();
        }
        return result;
    }


    public String getDrawerBackground(String path){
        String result = "";
        File main = new File(path);
        try{
            BufferedReader br = new BufferedReader(new FileReader(main));
            String line;
            while((line = br.readLine()) != null){
                if(line.contains("template.drawer_background=")){
                    result = line.replace("template.drawer_background=", "");
                    String afterHex = result.replace("#","");
                    return result;
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }

        return result;
    }

    public String getDrawerForeground(String path){
        String result = "";
        File main = new File(path);
        try{
            BufferedReader br = new BufferedReader(new FileReader(main));
            String line;
            while((line = br.readLine()) != null){
                if(line.contains("template.drawer_foreground=")){
                    result = line.replace("template.drawer_foreground=", "");
                    String afterHex = result.replace("#","");
                    return result;
                }
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }

        return result;

    }
}
