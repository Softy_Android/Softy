package com.MApps.Launcher.MiniUI.Settings;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.Window;

import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.R;
import com.jrummyapps.android.colorpicker.ColorPreference;

/**
 * Created by Admin on 5/6/2017.
 */
public class Drawer extends PreferenceActivity implements Preference.OnPreferenceClickListener
{
    @Override
    public void onCreate(Bundle bundle)
    {
        String theme = getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).getString(Data.TEMP_THEME, "");
        switch(theme)
        {
            case Data.LIGHT:
                setTheme(R.style.Light);
                break;

            case Data.DARK:
                setTheme(R.style.Dark);
                break;

            case "":
                setTheme(R.style.Dark);
                break;
        }
        super.onCreate(bundle);
        addPreferencesFromResource(R.xml.drawer);

        ((ColorPreference) findPreference("background")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putInt(Data.Drawer.DRAWER_BACKGROUND, (Integer) newValue).commit();
                return false;
            }
        });

        ((ColorPreference) findPreference("foreground")).setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
            @Override
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                getSharedPreferences(Data.NAME, Context.MODE_PRIVATE).edit().putInt(Data.Drawer.DRAWER_FOREGROUND, (Integer) newValue).commit();
                return false;
            }
        });
    }

    public void setToolbar(@Nullable Toolbar toolbar)
    {
        get().setSupportActionBar(toolbar);
    }

    private AppCompatDelegate acd;
    public AppCompatDelegate get()
    {
        if(acd == null)
        {
            acd = AppCompatDelegate.create(this, null);
        }
        return acd;
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        return false;
    }

}