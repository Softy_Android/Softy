package com.MApps.Launcher.MiniUI.Receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.MApps.Launcher.MiniUI.Classes.BroadcastReceiver;
import com.MApps.Launcher.MiniUI.Classes.Templates;
import com.MApps.Launcher.MiniUI.Data;
import com.MApps.Launcher.MiniUI.Mini;
import com.MApps.Launcher.MiniUI.MiniSettings;
import com.MApps.Launcher.MiniUI.R;
import com.MApps.Launcher.MiniUI.Template;
import com.lb.launcher.AppsCustomizePagedView;
import com.lb.launcher.AppsCustomizeTabHost;
import com.lb.launcher.BubbleTextView;
import com.lb.launcher.Hotseat;
import com.lb.launcher.Launcher;
import com.lb.launcher.LauncherRootView;
import com.lb.launcher.themes.IconPackManager;

import java.io.File;
import java.util.List;

/**
 * Created by Admin on 4/19/2017.
 */

public class a extends BroadcastReceiver {
    private Context c;
    @Override
    public void onReceive(Context context, Intent intent) {
        this. c = context;
        SharedPreferences sharedPrefs = context.getSharedPreferences(Data.NAME, Context.MODE_PRIVATE);
        int back = 0;
        if(sharedPrefs.getInt(Data.Drawer.DRAWER_BACKGROUND, 0) == 0 )
        {
            back = context.getColor(R.color.white);
        }else
        {
            back = sharedPrefs.getInt(Data.Drawer.DRAWER_BACKGROUND, 0);
        }
        int fore = 0;
        if(sharedPrefs.getInt(Data.Drawer.DRAWER_FOREGROUND, 0) == 0)
        {
            fore = context.getColor(R.color.dark);
        }else
        {
            fore = sharedPrefs.getInt(Data.Drawer.DRAWER_FOREGROUND, 0);
        }
        Launcher.mAppsCustomizeContent.setBackgroundColor(back);
        isColorDark(back, Launcher.mAppsCustomizeContent);
        Launcher.mAppsCustomizeTabHost.setBackgroundColor(fore);
    }
    public void isColorDark(int color, AppsCustomizePagedView mAppsCustomizeContent){
        double darkness = 1-(0.299*Color.red(color) + 0.587*Color.green(color) + 0.114*Color.blue(color))/255;
        if(darkness<0.5){
            mAppsCustomizeContent.getIcon().setTextColor(Color.BLACK);
        }else{
            mAppsCustomizeContent.getIcon().setTextColor(Color.WHITE);
        }
    }

}
